from fastapi import FastAPI

from models.database import engine
from models.models import Base
from routers import device, device_type, reading, status_device

Base.metadata.create_all(bind=engine)
app = FastAPI()


app.include_router(device.router)
app.include_router(reading.router)
app.include_router(device_type.router)
app.include_router(status_device.router)


@app.get("/")
async def root():
    return {"message": "power_plant root"}

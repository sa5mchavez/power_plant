# Wolf e commerce code challenge

Code challenge based in a power generation plant, which is responsible for monitoring the current energy of a device

## Getting started

Make sure you have installed

- [Python 3.9+](https://www.python.org/downloads/)
- [venv](https://pypi.org/project/virtualenv/)

## Installation


Setup your workspace:

```sh
git clone https://gitlab.com/sa5mchavez/power_plant.git power_plant
cd power_plant
make install
```


This action create a environment and configured locally
and now can be active the enviroment
```sh
source .venv/bin/activate
```


## Run

Can execute on locally

```sh
uvicorn main:app --reload 
```

This action wakes up the server on port 8000 and can be consumed at localhost:8000


The documentation is in the following link


http://localhost:8000/docs#/


## Need use on header "x-token" with the value "secret-token"

from datetime import datetime
from decimal import Decimal
from typing import Optional

from pydantic import BaseModel


class LuDeviceTypeBase(BaseModel):
    name: str


class LuDeviceTypeCreate(LuDeviceTypeBase):
    pass


class LuDeviceType(LuDeviceTypeBase):
    id: int

    class Config:
        orm_mode = True


class LuStatusDeviceBase(BaseModel):
    name: str


class LuStatusDeviceCreate(LuStatusDeviceBase):
    pass


class LuStatusDevice(LuStatusDeviceBase):
    id: int

    class Config:
        orm_mode = True


class DeviceBase(BaseModel):
    name: str
    current_power: Decimal
    device_type_id: int
    status_device_id: int

    created_at: Optional[datetime]
    updated_at: Optional[datetime]


class DeviceCreate(DeviceBase):
    pass


class Device(DeviceBase):
    id: int

    class Config:
        orm_mode = True


class ReadingBase(BaseModel):
    device_id: int
    device_type_id: int
    current_power: Decimal
    created_at: Optional[datetime]


class ReadingCreate(ReadingBase):
    pass


class Reading(ReadingBase):
    id: int

    class Config:
        orm_mode = True

from sqlalchemy import Column, DefaultClause, Double, ForeignKey, Integer, String
from sqlalchemy.sql import func
from sqlalchemy.types import DateTime

from .database import Base


class LuDeviceTypeModel(Base):
    __tablename__ = "LU_DEVICE_TYPE"

    id = Column(Integer, primary_key=True)
    name = Column(String)


class LuStatusDeviceModel(Base):
    __tablename__ = "LU_STATUS_DEVICE"

    id = Column(Integer, primary_key=True)
    name = Column(String)


class DeviceModel(Base):
    __tablename__ = "DEVICE"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    current_power = Column(Double)
    device_type_id = Column(Integer, ForeignKey(LuDeviceTypeModel.id))
    status_device_id = Column(Integer, ForeignKey(LuStatusDeviceModel.id))

    created_at = Column(DateTime, DefaultClause(func.now()), nullable=True)
    updated_at = Column(
        DateTime, DefaultClause(func.now(), for_update=True), nullable=True
    )


class ReadingModel(Base):
    __tablename__ = "READING"

    id = Column(Integer, primary_key=True)
    device_id = Column(Integer, ForeignKey(DeviceModel.id))
    device_type_id = Column(Integer, ForeignKey(LuDeviceTypeModel.id))
    current_power = Column(Double)

    created_at = Column(DateTime, DefaultClause(func.now()), nullable=True)

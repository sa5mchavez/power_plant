from typing import List, Type

from sqlalchemy.orm import Session

from models.models import LuStatusDeviceModel
from models.schemas import LuStatusDeviceCreate


def get_all_status(db: Session) -> List[Type[LuStatusDeviceModel]]:
    return db.query(LuStatusDeviceModel).all()


def get_status_by_id(status_id: int, db: Session) -> LuStatusDeviceModel:
    return db.query(LuStatusDeviceModel).get(status_id)


def create_status(
    status_device: LuStatusDeviceCreate, db: Session
) -> LuStatusDeviceModel:
    new_status_device = LuStatusDeviceModel(**status_device.model_dump())
    db.add(new_status_device)
    db.commit()
    db.refresh(new_status_device)
    return new_status_device


def update_status(
    status_type: LuStatusDeviceModel,
    status_type_input: LuStatusDeviceCreate,
    db: Session,
) -> LuStatusDeviceModel:

    status_type.name = status_type_input.name

    db.merge(status_type)
    db.commit()
    db.refresh(status_type)
    return status_type

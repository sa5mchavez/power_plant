from typing import List, Type

from sqlalchemy.orm import Session

from models.models import LuDeviceTypeModel
from models.schemas import LuDeviceTypeCreate


def get_all_device_types(db: Session) -> List[Type[LuDeviceTypeModel]]:
    return db.query(LuDeviceTypeModel).all()


def get_device_type_by_id(device_type_id: int, db: Session) -> LuDeviceTypeModel:
    return db.query(LuDeviceTypeModel).get(device_type_id)


def create_device_type(
    device_type: LuDeviceTypeCreate, db: Session
) -> LuDeviceTypeModel:
    new_device_type = LuDeviceTypeModel(**device_type.model_dump())
    db.add(new_device_type)
    db.commit()
    db.refresh(new_device_type)
    return new_device_type


def update_device_type(
    device_type: LuDeviceTypeModel, device_type_input: LuDeviceTypeCreate, db: Session
) -> LuDeviceTypeModel:

    device_type.name = device_type_input.name

    db.merge(device_type)
    db.commit()
    db.refresh(device_type)
    return device_type

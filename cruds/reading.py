from datetime import datetime
from typing import List, Type

from sqlalchemy.orm import Session

from models.models import ReadingModel
from models.schemas import ReadingCreate


def get_all_reading(db: Session) -> List[Type[ReadingModel]]:
    return db.query(ReadingModel).all()


def get_reading_by_id(reading_id: int, db: Session) -> ReadingModel:
    return db.query(ReadingModel).get(reading_id)


def create_reading(reading: ReadingCreate, db: Session) -> ReadingModel:
    new_reading = ReadingModel(**reading.model_dump())
    db.add(new_reading)
    db.commit()
    db.refresh(new_reading)
    return new_reading


def update_reading(
    reading: ReadingModel,
    reading_input: ReadingCreate,
    db: Session,
) -> ReadingModel:

    reading.current_power = reading_input.current_power
    reading.device_type_id = reading_input.device_type_id
    reading.updated_at = datetime.now()

    db.merge(reading)
    db.commit()
    db.refresh(reading)
    return reading


def get_all_reading_by_device_id(
    device_id: int, db: Session
) -> List[Type[ReadingModel]]:
    return db.query(ReadingModel).filter(ReadingModel.device_type_id == device_id).all()

from datetime import datetime
from decimal import Decimal
from typing import List, Type

from sqlalchemy.orm import Session

from models.models import DeviceModel
from models.schemas import DeviceCreate


def get_all_device(db: Session) -> List[Type[DeviceModel]]:
    return db.query(DeviceModel).all()


def get_device_by_id(device_id: int, db: Session) -> DeviceModel:
    return db.query(DeviceModel).get(device_id)


def create_device(device: DeviceCreate, db: Session) -> DeviceModel:
    new_device = DeviceModel(**device.model_dump())
    db.add(new_device)
    db.commit()
    db.refresh(new_device)
    return new_device


def update_device(
    device: DeviceModel,
    device_input: DeviceCreate,
    db: Session,
) -> DeviceModel:

    device.name = device_input.name
    device.current_power = device_input.current_power
    device.device_type_id = device_input.device_type_id
    device.status_device_id = device_input.status_device_id
    device.updated_at = datetime.now()

    db.merge(device)
    db.commit()
    db.refresh(device)
    return device


def update_current_power(
    device: DeviceModel,
    current_power: Decimal,
    db: Session,
) -> DeviceModel:
    device.current_power = current_power
    device.updated_at = datetime.now()

    db.merge(device)
    db.commit()
    db.refresh(device)
    return device


def get_devices_by_device_type_id(
    device_type_id: int, db: Session
) -> List[Type[DeviceModel]]:
    return (
        db.query(DeviceModel).filter(DeviceModel.device_type_id == device_type_id).all()
    )

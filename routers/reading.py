from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from cruds.device import get_device_by_id, update_current_power
from cruds.reading import (
    create_reading,
    get_all_reading,
    get_all_reading_by_device_id,
    get_reading_by_id,
    update_reading,
)
from dependencies import get_db, get_token_header
from models.schemas import Reading, ReadingCreate

router = APIRouter(
    prefix="/reading",
    tags=["reading"],
    dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[Reading])
async def read_all_reading(db: Session = Depends(get_db)):
    return get_all_reading(db)


@router.get("/{reading_id}", response_model=Reading)
async def read_reading_by_id(reading_id: int, db: Session = Depends(get_db)):

    reading = get_reading_by_id(reading_id, db)

    if not reading:
        raise HTTPException(status_code=404, detail="reading_id not found")

    return reading


@router.post("/", response_model=Reading)
async def new_reading(reading: ReadingCreate, db: Session = Depends(get_db)):
    reading_create = create_reading(reading, db)
    device = get_device_by_id(reading_create.device_id, db)
    update_current_power(device, reading_create.current_power, db)

    return reading_create


@router.put("/{reading_id}", response_model=Reading)
async def update_reading_by_id(
    reading_id: int,
    reading_input: ReadingCreate,
    db: Session = Depends(get_db),
):
    reading = get_reading_by_id(reading_id, db)

    if not reading:
        raise HTTPException(status_code=404, detail="reading_id not found")

    try:
        return update_reading(reading, reading_input, db)

    except Exception as e:
        raise HTTPException(status_code=403, detail=f"error updating: {str(e)}")


@router.get("/device/{device_id}", response_model=List[Reading])
async def read_all_reading_by_device_id(device_id: int, db: Session = Depends(get_db)):
    return get_all_reading_by_device_id(device_id, db)


@router.get("/total-power/{device_id}")
async def get_total_power_by_device_id(device_id: int, db: Session = Depends(get_db)):
    readings_by_device = get_all_reading_by_device_id(device_id, db)

    total_power = {
        "device_id": device_id,
        "power": 0.0,
    }

    total = 0
    for read in readings_by_device:
        if read.current_power > 0:
            total += read.current_power

    total_power["power"] = total
    return total_power

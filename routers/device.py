from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from cruds.device import (
    create_device,
    get_all_device,
    get_device_by_id,
    get_devices_by_device_type_id,
    update_device,
)
from dependencies import get_db, get_token_header
from models.schemas import Device, DeviceCreate

router = APIRouter(
    prefix="/device",
    tags=["device"],
    dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[Device])
async def read_all_device(db: Session = Depends(get_db)):
    return get_all_device(db)


@router.get("/{device_id}", response_model=Device)
async def read_device_id(device_id: int, db: Session = Depends(get_db)):

    device = get_device_by_id(device_id, db)

    if not device:
        raise HTTPException(status_code=404, detail="device_id not found")

    return device


@router.post("/", response_model=Device)
async def new_device(device: DeviceCreate, db: Session = Depends(get_db)):
    return create_device(device, db)


@router.put("/{device_id}", response_model=Device)
async def update_device_by_id(
    device_id: int,
    device_input: DeviceCreate,
    db: Session = Depends(get_db),
):
    device = get_device_by_id(device_id, db)

    if not device:
        raise HTTPException(status_code=404, detail="device_id not found")

    try:
        return update_device(device, device_input, db)

    except Exception as e:
        raise HTTPException(status_code=403, detail=f"error updating: {str(e)}")


@router.get("/device-type/{device_type_id}", response_model=List[Device])
async def read_devices_by_device_type_id(
    device_type_id: int, db: Session = Depends(get_db)
):
    return get_devices_by_device_type_id(device_type_id, db)

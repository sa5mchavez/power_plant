from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from cruds.device_type import (
    create_device_type,
    get_all_device_types,
    get_device_type_by_id,
    update_device_type,
)
from dependencies import get_db, get_token_header
from models.schemas import LuDeviceType, LuDeviceTypeCreate

router = APIRouter(
    prefix="/device-type",
    tags=["devices-type"],
    dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[LuDeviceType])
async def read_devices(db: Session = Depends(get_db)):
    return get_all_device_types(db)


@router.get("/{device_type_id}", response_model=LuDeviceType)
async def read_device_id(device_type_id: int, db: Session = Depends(get_db)):

    device_type = get_device_type_by_id(device_type_id, db)

    if not device_type:
        raise HTTPException(status_code=404, detail="device_type_id not found")

    return device_type


@router.post("/", response_model=LuDeviceType)
async def new_device_type(
    device_type: LuDeviceTypeCreate, db: Session = Depends(get_db)
):
    return create_device_type(device_type, db)


@router.put("/{device_type_id}", response_model=LuDeviceType)
async def update_device_type_by_id(
    device_type_id: int,
    device_type_input: LuDeviceTypeCreate,
    db: Session = Depends(get_db),
):
    device_type = get_device_type_by_id(device_type_id, db)

    if not device_type:
        raise HTTPException(status_code=404, detail="device_type_id not found")

    try:
        return update_device_type(device_type, device_type_input, db)

    except Exception as e:
        raise HTTPException(status_code=403, detail=f"error updating: {str(e)}")

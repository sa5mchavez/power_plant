from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from cruds.status_device import (
    create_status,
    get_all_status,
    get_status_by_id,
    update_status,
)
from dependencies import get_db, get_token_header
from models.schemas import LuStatusDevice, LuStatusDeviceCreate

router = APIRouter(
    prefix="/status-device",
    tags=["status-device"],
    dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[LuStatusDevice])
async def read_status(db: Session = Depends(get_db)):
    return get_all_status(db)


@router.get("/{status_id}", response_model=LuStatusDevice)
async def read_status_id(status_id: int, db: Session = Depends(get_db)):

    status = get_status_by_id(status_id, db)

    if not status:
        raise HTTPException(status_code=404, detail="status_type_id not found")

    return status


@router.post("/", response_model=LuStatusDevice)
async def new_status(device_type: LuStatusDeviceCreate, db: Session = Depends(get_db)):
    return create_status(device_type, db)


@router.put("/{status_id}", response_model=LuStatusDevice)
async def update_status_by_id(
    status_id: int,
    status_device_input: LuStatusDeviceCreate,
    db: Session = Depends(get_db),
):
    status = get_status_by_id(status_id, db)

    if not status:
        raise HTTPException(status_code=404, detail="status_type_id not found")

    try:
        return update_status(status, status_device_input, db)

    except Exception as e:
        raise HTTPException(status_code=403, detail=f"error updating: {str(e)}")
